package ru.tinkoff.favouritepersons

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.stubbing.Scenario
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.pages.EditPersonScreen
import ru.tinkoff.favouritepersons.pages.FavouritePersonsScreen
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.favouritepersons.rules.LocalhostPreferenceRule
import ru.tinkoff.favouritepersons.utils.fileToString


class FavoutitePersonsTest {

    private lateinit var db: PersonDataBase

    @get: Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    @Test
    fun clickAddNewPerson() {
        stubFor(
            get(urlPathMatching("/api/"))
                .willReturn(
                    ok(fileToString("mock/miss_eliza.json"))
                )
        )
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabDownloadMatchers)
            noPersons.isNotDisplayed()
        }
    }

    @Test
    fun deletePerson() {
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs(Scenario.STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    ok(fileToString("mock/miss_tverdogosta.json"))
                )
        )
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs("Step 1")
                .willSetStateTo("Step 2")
                .willReturn(
                    ok(fileToString("mock/miss_eliza.json"))
                )
        )
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs("Step 2")
                .willSetStateTo("Step finish")
                .willReturn(
                    ok(fileToString("mock/miss_sedef.json"))
                )
        )
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabDownloadMatchers)
            clickElement(fabDownloadMatchers)
            clickElement(fabDownloadMatchers)
            swipeLeft(elizaMatcher)
            onView(elizaMatcher).check(doesNotExist())
        }
    }

    @Test
    fun sortedTest() {
        FavouritePersonsScreen {
            clickElement(sortedMatcher)
            elementIsChecked(sortDefault)
        }
    }

    // нет проверки
    @Test
    fun sortedByAge() {
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs(Scenario.STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    ok(fileToString("mock/miss_tverdogosta.json"))
                )
        )
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs("Step 1")
                .willSetStateTo("Step 2")
                .willReturn(
                    ok(fileToString("mock/miss_eliza.json"))
                )
        )
        stubFor(
            get(urlPathMatching("/api/"))
                .inScenario("Miss")
                .whenScenarioStateIs("Step 2")
                .willSetStateTo("Step finish")
                .willReturn(
                    ok(fileToString("mock/miss_sedef.json"))
                )
        )
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabDownloadMatchers)
            clickElement(fabDownloadMatchers)
            clickElement(fabDownloadMatchers)
            clickElement(sortedMatcher)
            clickElement(sortAge)
        }
    }

    @Test
    fun checkPersonData() {
        stubFor(
            get(urlPathMatching("/api/"))
                .willReturn(
                    ok(fileToString("mock/miss_eliza.json"))
                )
        )
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabDownloadMatchers)
            clickElement(elizaMatcher)
        }
        EditPersonScreen {
            checkText(name, "Eliza")
            checkText(surname, "Fitzgerald")
            checkText(gender, "Ж")
            checkText(birthdate, "1985-07-09")
        }
    }

    @Test
    fun editPersonData() {
        stubFor(
            get(urlPathMatching("/api/"))
                .willReturn(
                    ok(fileToString("mock/miss_eliza.json"))
                )
        )
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabDownloadMatchers)
            clickElement(elizaMatcher)
        }
        EditPersonScreen {
            typeText(name, "Иосиф")
            clickElement(saveButton)
            Thread.sleep(1000)
        }
        FavouritePersonsScreen {
            checkText(personName, "Иосиф Fitzgerald")
        }
    }

    @Test
    fun manuallyAddPerson() {
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabAddPersonManually)
        }
        EditPersonScreen {
            typeText(name, "Иосиф")
            typeText(surname, "Сталин")
            typeText(gender, "М")
            typeText(birthdate, "1953-05-03")
            typeText(email, "stalin@moscow.ussr")
            typeText(phone, "881200000000")
            typeText(address, "Moscow, Kremlin")
            typeText(
                imageLink,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Iosif_Stalin.jpg/500px-Iosif_Stalin.jpg"
            )
            typeText(scope, "88")
            clickElement(saveButton)
        }
        FavouritePersonsScreen {
            checkText(personName, "Иосиф Сталин")
            checkText(personPrivateInfo, "Male, 70")
        }
    }

    @Test
    // хз как
    fun errorText() {
        FavouritePersonsScreen {
            clickElement(fabAddPersonMatcher)
            clickElement(fabAddPersonManually)
        }
        EditPersonScreen {
            clickElement(saveButton)
        }
    }
}