package ru.tinkoff.favouritepersons.pages

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.Matcher

open class BaseScreen {

    fun clickElement(element: Matcher<View>) {
        onView(element)
            .perform(ViewActions.click())
    }

    fun checkText(matcher: Matcher<View>, text: String) {
        onView(matcher)
            .check(ViewAssertions.matches(ViewMatchers.withText(text)))
    }

    fun containText(matcher: Matcher<View>, text: String) {
        onView(matcher)
            .check(ViewAssertions.matches(ViewMatchers.withContentDescription(text)))
    }
}