package ru.tinkoff.favouritepersons.pages

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import ru.tinkoff.favouritepersons.R

class EditPersonScreen: BaseScreen() {
    val name: Matcher<View> = withId(R.id.et_name)
    val surname: Matcher<View> = withId(R.id.et_surname)
    val gender: Matcher<View> = withId(R.id.et_gender)
    val birthdate: Matcher<View> = withId(R.id.et_birthdate)
    val email: Matcher<View> = withId(R.id.et_email)
    val phone : Matcher<View> = withId(R.id.et_phone)
    val address: Matcher<View> = withId(R.id.et_address)
    val imageLink: Matcher<View> = withId(R.id.et_image)
    val scope: Matcher<View> = withId(R.id.et_score)
    val saveButton: Matcher<View> = withId(R.id.submit_button)

    fun typeText(matcher: Matcher<View>, text: String) {
        onView(matcher)
            .perform(ViewActions.replaceText(text))
    }

    companion object {
        inline operator fun invoke(crossinline block: EditPersonScreen.() -> Unit) =
            EditPersonScreen().block()
    }
}