package ru.tinkoff.favouritepersons.pages

import android.view.View
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import io.github.kakaocup.kakao.text.KTextView
import androidx.test.espresso.action.ViewActions.swipeLeft
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import ru.tinkoff.favouritepersons.R
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isChecked

class FavouritePersonsScreen : BaseScreen() {

    val fabAddPersonMatcher: Matcher<View> = withId(R.id.fab_add_person)
    val fabDownloadMatchers: Matcher<View> = withId(R.id.fab_add_person_by_network)
    val sortedMatcher: Matcher<View> = withId(R.id.action_item_sort)
    val sortAge: Matcher<View> = withId(R.id.bsd_rb_age)
    val personName: Matcher<View> = withId(R.id.person_name)
    val fabAddPersonManually: Matcher<View> = withId(R.id.fab_add_person_manually)
    val personPrivateInfo: Matcher<View> = withId(R.id.person_private_info)

    val noPersons = KTextView { withId(R.id.tw_no_persons) }

    val sortDefault: Matcher<View> = Matchers.allOf(
        Matchers.instanceOf(AppCompatRadioButton::class.java),
        withId(R.id.bsd_rb_default)
    )

    val elizaMatcher = Matchers.allOf(
        Matchers.instanceOf(AppCompatTextView::class.java),
        withId(R.id.person_name),
        withText("Eliza Fitzgerald")
    )

    fun elementIsChecked(element: Matcher<View>) {
        onView(element).check(matches(isChecked()))
    }

    fun swipeLeft(nameMatcher: Matcher<View>) {
        onView(nameMatcher).perform(swipeLeft())
    }

    companion object {
        inline operator fun invoke(crossinline block: FavouritePersonsScreen.() -> Unit) =
            FavouritePersonsScreen().block()
    }
}
